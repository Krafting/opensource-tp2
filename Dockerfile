FROM eclipse-temurin:17-jdk-jammy

WORKDIR /app

COPY ./MavenHelloWorld-0.2.0.jar ./

CMD ["java", "-jar", "MavenHelloWorld-0.2.0.jar"]