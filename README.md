# MavenHelloWorld
Sample Maven hello world new


# TP1 : Git

# README

# Git init

```shell
git init
git add .
git commit -m "message"
git remote add origin git@gitlab.com:krafting/repo.git
```

# Git checkout

```shell
# Revenir à la version d'un fichier d'un commit précédent
git checkout <commit_id> path/to/file.sh
```

# Git branch

```shell
# Liste branches:
git branch *

# Créer une branche:
git branch <name>

# Switch sur une branch:
git checkout <branch>

# Merge une branche:
git checkout <branch_destination>
git merge <branch_source>

# Merge un commit specifique
git log 
git checkout <brach_destination>
git merge <commit_id>

# Supprimer une branche
git branch -d <branche>
```

# Git commit

```shell
# Ajoute les fichiers et commit directement
git commit -am "Modifs"
```

# Git ammend

```shell
# Modif du dernier commit
git commit -m "Message"
git commit --ammend -m "Modif du message"
```

# Git revert

```shell
# Apres avoir fais des commit:
git revert HEAD^
```

# Git reset

```shell
# Pas compris celui la ??
git reset <file>

# Retire les commit mais ne supprime pas les modifications faites (ex: un fichier ajouter restera la)
git reset --soft <CommitID>

# Supprime le commit.
git reset <CommitID>

# Retire les commit et supprime les modifications qui ont été faites
git reset --hard <CommitID>
```

# Git stash

```shell
# Mettre les modifs de coté, pour les rajouter sur une branche apres
git stash 

# Appliqué les changements stashé sur une nouvelle branche
git stash apply

# List des stashs
git stash list

# Appliqué un stash particulier
git stash apply <NuméroStash>
```

# Git reflog & log

Reflog log toutes les actions, les reset etc..

```shell
git reflog

# Connaitre les modifs d'un certain fichier
git log <File>
```

# Conflit

```sh
# Resoudre les conflits et garder la version distante:
git checkout --theirs <file>

# Resoudre les conflits et garder la version local
git checkout --ours <file>
```